/* Function which remove attributes (name, placeholder) and description for input field */

function removeAttribute() {
  document.querySelector('.size').removeAttribute('name');
  document.querySelector('.size').removeAttribute('placeholder');
  document.querySelector('.description').innerHTML = '';
}

/* Function which make new attribute for input field (deponds on our request) */

function setName(value) {
  document.querySelector('.size').setAttribute('name', value);
}

function setPlaceholder(value) {
document.querySelector('.size').setAttribute("placeholder", value);
}

function setDescription(value) {
document.querySelector('.description').innerHTML = value;
}

/* function which make additional input field (for furniture in our case) */

function furnitureSize(value) {
  var size = document.createElement("input");
  size.classList.add('form-control');
  size.classList.add(value);
  size.style.margin = '10px 0';
  size.setAttribute('name', value);
  size.setAttribute("placeholder", 'Enter ' + value + ' here');
  var el = document.querySelector('.input_table');
  var child = document.querySelector('.weight');
  el.insertBefore(size, child);
}

/* function wich change product type description  */

function changeHelper(value) {
  document.querySelector(".helper").innerHTML = value;
} 



/* function addNote() {
  var size = document.createElement("p");
  var el = document.querySelector('.input_table');
  var child = document.querySelector('.weight');
  el.insertBefore(size, child);
} */


/* function which remove additional input fields */

function removeFurniture() {
  document.querySelector('.lenght').remove();
  document.querySelector('.height').remove();
}


/* Product type function */

function letNewIntupWork() {

document.querySelector('.type').addEventListener('change', function () {

  switch (this.value) {

    case 'dvd':
      changeHelper('Size should be provided in MB (Megabytes)');
      removeAttribute();
      setName('size');
      setPlaceholder('Enter size here');
      setDescription('Product size:');
      removeFurniture();

      break;

    case 'book':
      changeHelper('Book weight should be providet in kg (kilograms)');
      removeAttribute();
      setName('weight');
      setPlaceholder('Enter weight here');
      setDescription('Product weight:');
      removeFurniture();

      break;

    case 'furniture':
      changeHelper('Furniture dimension should be provided in cm (centimeters)');
      removeAttribute();
      setName('width');
      setPlaceholder('Enter width here');
      setDescription('Product dimensions:');
      furnitureSize('lenght');
      furnitureSize('height');

      break;

      }
  

});

}

letNewIntupWork();