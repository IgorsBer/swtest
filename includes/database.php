<?php 

require_once('config.php');

class Database {

  public $host = DB_HOST;
  public $user = DB_USER;
  public $pass = DB_PASS;
  public $db_name = DB_NAME;

  public $link;

  function __construct(){

    $this->connection();
    
  }

  /* function which return connection to DB */

  private function connection() {

    $this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  }

  /* query SELECT function which return SELECT request from DB */

  public function select($sql) {

    $result = $this->link->query($sql);

    if($result->num_rows > 0) {

      return $result;

    } else {

      return false;

    }

  }

  /* query INSERT function which return INSERT request from DB */

  public function insert($sql) {

    $insert = $this->link->query($sql);

    if($insert) {

      echo "<script>alert('Product added succesfully')</script>";

    } else {

      echo "<script>alert('Please fill all form fields!')</script>";

    }

  } 

  /* query DELETE function which return DELETE request from DB */

  public function delete($query) {

    $delete = $this->link->query($query);

    if($delete) {

      header('location: index.php?msg= Unit deleted');

    } else {

      echo "Unit have not deleted";

    }

  }

}