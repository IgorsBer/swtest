<?php 

include ('views/header.php');

/* making SELECCT OPTION variable */

$option = $_POST['type'];

/* changing PHP script depenfing on selected option */

switch ($option) {

    case 'dvd';
      include ('includes/add_product_dvd.php');
      break;

    case 'book';
      include ('includes/add_product_book.php');
      break;
      
    case 'furniture';
      include ('includes/add_product_furniture.php');
      break;

}





?>


<div class="container box">
<form action="add_product.php" method="post">

  <div class="form-group">
    <select name="type" id="type" class="form-control type">
      <option value="dvd" selected>DVD</option>
      <option value="book">BOOK</option>
      <option value="furniture" >FURNITURE</option>
    </select>
    <br>
    <label>Model:</label>
    <input type="text" class="form-control model" name="model" placeholder="Enter model name">
  </div>
  <div class="form-group">
    <label>SKU</label>
    <input type="text" class="form-control sku" name="sku" placeholder="Enter SKU number">
  </div>
  <div class="form-group input_table">
    <label><p class="description">Size</p></label>
    <input type="text" class="form-control size" name="size" placeholder="Enter size">
  </div>
  <p class="helper">Size should be provided in MB (Megabytes)</p>
  <div class="form-group">
    <label>Price</label>
    <input type="text" class="form-control price" name="price" placeholder="Enter price">
  </div>
  <div class="btn-group" role="group" aria-label="Basic example">
    <button type="submit" name="submit" class="btn btn-success">Submit</button>
  </div>

</form>
</div>



<script src="js/main.js"></script>


</body>
<?php 

include ('views/footer.php');

?>