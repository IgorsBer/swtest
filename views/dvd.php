<?php 

/* connect to DB */

$db = new Database();

/* making query to DB */

$sql = "SELECT * FROM dvd order by id DESC";

/*  Selecting all units from books table using select function */

$dvd = $db->select($sql);

?>

<!-- fetchng units into HTML file -->

<form action="delete.php" method="post">
  <div class="table">
    <?php while($row = $dvd->fetch_array()) : ?>
      <div class="unit">
      <input type="checkbox" name="checkbox-dvd[]" value=<?php echo $row['id']; ?>>
          <p class="field-logo">DVD</p>
          <p class="field"><?php echo $row['model']; ?></p>
          <p class="field"><?php echo $row['sku']; ?></p>
          <p class="field">Size: <?php echo $row['size']; ?> MB</p>
          <p class="field">Price: <?php echo $row['price']; ?> &euro;</p>
      </div>
    <?php endwhile; ?>
  </div>


