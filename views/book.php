<?php 

/* connect to DB */

$db = new Database();

/* making query to DB */

$sql = "SELECT * FROM books order by id DESC";

/*  Selecting all units from books table using select function */

$book = $db->select($sql);


?>

<!-- fetchng units into HTML file -->

<div class="table">
  <?php while($row_1 = $book->fetch_array()) : ?>
    <div class="unit">
    <input type="checkbox" name="checkbox-book[]" value=<?php echo $row_1['id']; ?>>
        <p class="field-logo">Book</p>
        <p class="field"><?php echo $row_1['model']; ?></p>
        <p class="field"><?php echo $row_1['sku']; ?></p>
        <p class="field">Weight: <?php echo $row_1['weight']; ?> kg</p>
        <p class="field">Price: <?php echo $row_1['price']; ?> &euro; </p>
    </div>
  <?php endwhile; ?>
  </div>


