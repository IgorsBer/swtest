<?php 

/* connect to DB */

$db = new Database();

/* making query to DB */

$sql = "SELECT * FROM furniture order by id DESC";

/*  Selecting all units from books table using select function */

$table = $db->select($sql);

?>

<!-- fetchng units into HTML file -->

<div class="table">
  <?php while($row_2 = $table->fetch_array()) : ?>
    <div class="unit">
    <input type="checkbox" name="checkbox-table[]" value=<?php echo $row_2['id']; ?>>
        <p class="field-logo">FURNITURE</p>
        <p class="field"><?php echo $row_2['model']; ?></p>
        <p class="field"><?php echo $row_2['sku']; ?></p>
        <p class="field">Dimension: <?php echo $row_2['width']; ?>x<?php echo $row_2['lenght']; ?>x<?php echo $row_2['height']; ?>cm</p>
        <p class="field">Price: <?php echo $row_2['price']; ?> &euro;</p>
    </div>
    <?php endwhile; ?>
  </div>
  <button type="submit" name="submit" class="btn btn-danger btn-delete">DELETE</button>
</form>
  