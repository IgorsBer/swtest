<?php 

require_once ('includes/database.php');

$db = new Database();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <title>SW Test</title>
</head>
<body>
  <header class="mb-auto header">
    <div class="inner">
      <div class="logo"><img src="images/scandiweb_logo.png" alt="Logo" class="logo_img"></div>
      <nav class="nav justify-content-center">
        <a class="nav-link active" href="index.php">Home</a>
        <a class="nav-link" href="add_product.php">Add Products</a>
      </nav>
    </div>
  </header>

  